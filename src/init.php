<?php
\Larakit\Boot::register_migrations(__DIR__ . '/migrations');
if(!function_exists('me')) {
    function me($prop = null) {
        $user = Auth::user();
        if($prop) {
            return $user ? $user->{$prop} : null;
        }
        
        return $user;
    }
}

\Larakit\Twig::register_function('me', function ($prop = null) {
    return me($prop);
});